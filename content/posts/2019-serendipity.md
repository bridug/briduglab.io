Title: Infrastructure as Code
Date: 2019-12-22
Category: tech-ish
Tags: infrastructure,containers
Slug: infrastructure-as-code

Serendipity has always been a favorite word of mine, but I think I have been using it wrong [all] [these] [years].  It would have been great to be able to follow those links to my misuse of the word, but many of my ramblings are lost forever (or more likely living on forever) in a Federal Government data center.  Digression aside, most often I would holler **serendipity!** due to coincidences based on important aspects of my current situation.  Maybe I have always meant [synchronicity](https://www.askdifference.com/serendipity-vs-synchronicity/) because a grand plot/design would be way cooler.  Maybe they are just coincidences or even better [happy little accidents](https://www.goodreads.com/book/show/31934183-happy-little-accidents)

Today I exclaimed **serendipity!** when I found an article from Medium in the overwhelming onslaught of spam in my personal email.  [Terraform vs CloudFormation](https://medium.com/@endofcake/terraform-vs-cloudformation-1d9716122623) came right on time to remind me that I should **not** click to create a container registry for our current project.  Rather, I should build the definition in code such that it can be evaluated, recreated and becomes self-documenting (yeah right).  Either way, this was great timing by Medium, almost like they read my mind (or my cookies).