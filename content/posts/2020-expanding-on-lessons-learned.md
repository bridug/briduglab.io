Title: Expanding on Some Lessons Learned
Date: 2020-02-03
Category: tech-ish
Tags: learning
Slug: expanding-on-lessons-learned

One of the recurring themes in learning "modern" infrastructure practices is that information that is just 18 months old may be out dated.  I have found serveral practices as I dug into containerization that were supplanted by full features that were developed long ago, but live on due to examples being readily available (and of course because they work).  While I do not believe this is functionally any different than the bad-old-days, the pace is different.  Where a whole product ecosystem could spring up in the time it took a traditional IT cornerstone vendor to produce features, modern dev practices make streaming (vs dumping) features far more commonplace.

### RTFM
So, after 24 years in IT, I guess I am finally having to learn to read vendor documentation.  I have always used vendor documentation, but I usually augmented it with code samples, tutorials and examples I found online or in books.  Don't get me wrong, Stack Overflow and I are not in a fight or anything, but with the rate of change in core software means I really need to know _how_ to read documentation.

In my journey to be a better information consumer I have adopted the following:

1. Make the time to read the document, do not do it under stress of incident resolution or while distracted by something (anything) else.
1. Do not relegate the documentation to "reference guide"
1. Look for intent of design, "Can I see what they are trying to achieve while reading this?".  This helps me to tie learning together as it provides a purpose, beyond the knowledge.  It also helps me significantly with retention.
1. Turn off the inner voice that both reads words to me and gets distracted by people walking by, people using their hands to talk, any movement at all really, my IDE theme being wonky, dust on my desk, etc...  This has proven to be the hardest, but when I can do it, it is magical.
