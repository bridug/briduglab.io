Title: First Crack: Container Build Pipleline on Gitlab
Date: 2020-01-01 10:00
Category: tech-ish
Tags: containers,CI/CD
Slug: first-crack-container-build-pipeline

I feel like that guy who, [for some reason](https://youtu.be/dTllG2KdPMQ?t=55 "YouTube - Jumanji"), waits many years to see a movie or television show and then acts like it is brand new...actually I am that guy.  Yesterday => Early today, I created a CI/CD workflow to build an image and deposit it into [GitLab's Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/ "GitLab Container Registry Documentation").  It is not pretty, but along the way I learned a few things that I will jot down for future reference.

First, I went about this **all wrong**.  My initial steps are tainted by traditional data center practices:

1. Build a "base" Docker image for the project and upload it to the container registry.  More-or-less `apt-get update && apt-get upgrade`.  I was staunchly in a _but we need to conserve those precious [build minutes](https://about.gitlab.com/pricing/ "GitLab - Pricing Structure")!_ mindset)
1. Manually [install the runner](https://docs.gitlab.com/runner/install/ "GitLab runner installation instructions") on an EC2 instance
1. [Fight](https://youtu.be/et20u4PMTCc?t=22 "YouTube - StepBrothers kids fight scene") with that
1. Admit defeat, <s>go home</s> go back to the Shared Runners
1. Fiddle around with stuff until I can reference said image from my [.gitlab-ci.yml](https://hackernoon.com/configuring-gitlab-ci-yml-150a98e9765d "Hackernoon - GitLab CI File Explained")
1. Add stuff to the Dockerfile and try to build on top of it using build stages.  The key here is that Docker-in-Docker cannot [auto-cache](https://pythonspeed.com/articles/docker-caching-model/ "Basics of Docker Caching") at line checkpoints in the Dockerfile since the build artifacts are destroyed with the parent container.

Final **enlightened** process:

1. Construct a Dockerfile
1. Build a `.gitlab-ci.yml`
1. Commit both to `master`
1. Let the shared runners handle it and worry about the build minutes later

This is basically the [draw an owl meme](https://knowyourmeme.com/photos/572078-how-to-draw-an-owl "KnowYourMeme - How to Draw an Owl"), but I am crafting a post right now on the [final process list]({filename}2020-simple-docker-gitlab-ci.md)

_Full circle_ =( but it was an interesting learning experience.  No excuses, but much of my progress with these _generally accepted_ practices is slowed significantly by the beatings I took over clear text secrets (me: files and ENV variables and cmd line; Security Dude: oh my!) while working for the Federal Government.  Stashing secrets in plaintext is considered wrong by most, but passing secrets is a necessary evil and it is far less impactful when using a [strategy that assumes compromise](https://medium.com/clevyio/managing-secrets-in-ci-stages-with-aws-secrets-manager-7240938ddf41 "Medium - Secrets management with GitLab and AWS").  Plus, I feel like if I just keep doing it [without any repercussions](https://www.howtogeek.com/434930/why-are-companies-still-storing-passwords-in-plain-text/ "How To Geek - Plain Text Passwords Rundown") it will stop feeling so icky at some point...

Also, I really did not find any comprehensive "basic" tutorials on this and had to glue it together.  Relevant [XKCD](https://c.xkcd.com/random/comic/ "XKCD random comic")
![alt text](https://imgs.xkcd.com/comics/containers.png "XKCD Containers, six panel comic")
