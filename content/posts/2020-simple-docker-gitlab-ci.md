Title: Simple Docker / Gitlab CI
Date: 2020-01-01 12:00
Category: tech
Tags: containers,CI/CD
Slug: simple-docker-gitlab-ci

Well [dear readers](https://www.youtube.com/watch?v=CZayfRKD6n8 "YouTube - Brian / Peter / Mirror") I will finally post something with meat.  I wanted to document (outside of our private GitLab repo) the files I used to get going.  Completely inspired by [GitLab's sample project](https://gitlab.com/gitlab-examples/docker/tree/master "GitLab - Sample Docker project").

You can find the test project [here](https://gitlab.com/bridug/myproject "GitLab - MyProject Site") for the impatient.  Fork it and try for yourself.

### Dockerfile:

```docker
# specify the version of Ubuntu ensure reproducibility
FROM ubuntu:18.04

# update apt-cache and grab python2.7
RUN apt-get update && apt-get -y install \
    python2.7

# get rid of cached packages to help control image size
RUN apt-get clean

# add our deployment directory to the image so we have the requirements.txt files
ADD deployment deployment

# grab pip (which is complicated for python2.7)
ADD https://bootstrap.pypa.io/get-pip.py /tmp/get-pip.py

# install pip
RUN python2.7 /tmp/get-pip.py --user 

# install our python libraries
RUN python2.7 -m pip install -r /deployment/requirements.txt

# on container run, simply print a listing from the requirements.txt file
CMD /bin/bash -c "echo Displaying the contents of the requirements.txt file ; cat /deployment/requirements.txt"
```

### gitlab-ci.yml:

```yaml
# specify the docker image used to ensure reproducibility
image: docker:19.03.5
# user the Docker-in-Docker method to produce our docker image
services:
    - docker:dind
# login to the repository's container registry using a one time, auto-generated token
before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
# specify a job called build release
build-release:
    stage: build
    # actual meat of execution here
    script:
        # build our image with the attached dockerfile
        - docker build -t registry.gitlab.com/bridug/myproject -f deployment/Dockerfile .
        # upload resulting image on
        - docker push registry.gitlab.com/bridug/myproject
        # run the resulting container
        - docker run registry.gitlab.com/bridug/myproject
    # run this ONLY if we merge to the master branch    
    only:
        - master  
    # ensure our runner has the docker tag (capable of docker execution)
    tags:
        - docker
```

### Project Folder Configuration:
![alt text]({static}/images/2020-simple-docker-gitlab-ci_terminal-tree.png "Terminal - Tree View of Project")

### Abridged Runner Output
![alt text]({static}/images/2020-simple-docker-gitlab-ci_ci-output-snip.png "Snip - Runner Console")
