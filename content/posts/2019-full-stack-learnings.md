Title: Full Stack Learnings
Date: 2019-12-29
Category: tech-ish
Tags: complete stack,containers
Slug: full-stack-learnings

The methods we are currently investigating to reduce elapsed time between change / deployment and to increase developer(data scientist) accessibility in our project are in no way magical or mythical.  They are, however, things that _we_ have never done.  By _we_ I pretty much mean _me_, but it is true that no one in the organization has complete experience with what we are trying to accomplish.  To complicate things, the real goal is not just a production quality Data Sciencey application; but to create an extensible, reusable framework for future activities of this ilk.

Enter the [Mythical Mysfits](https://aws.amazon.com/getting-started/projects/build-modern-app-fargate-lambda-dynamodb-python/) modern web app tutorial through AWS.  The tutorial has the "developer" growing the app from a static hosted site, to a fully integrated web application complete with load balancing, containers, CI/CD, database backend, user logins and user click capture.  The tutorial was pretty [magical](https://youtu.be/c_8yTbNpESE?t=42) to me, in part because I have always wished I could create a learning device this comprehensive.  But also because with the tools available to us today we can expose our ideas without having tons of infrastructure knowledge or going through the trouble of finding a [guy](https://www.youtube.com/watch?v=W8_Kfjo3VjU)...  

_Who am I kidding_.  The tutorial was enlightening (and really fun) for sure, but the biggest takeaway for me is that AWS is a crazy complex ecosystem.  There are at least as many ways to do something as there are services with the word "Elastic" in the title.  Honestly, the word _elastic_ should be occupy a square in [buzzword bingo](https://en.wikipedia.org/wiki/Buzzword_bingo).

Deploying a professional service to 2020's standards will require iteration and growth through failure (which is usually the best option for learning as an organization), or if we are risk averse: working with someone that has the experience to translate our technical "requirements" into immediate public cloud realities.

So [what did you learn](https://www.youtube.com/watch?v=hCPgTZgRQHc)?

1. You may wish to create a separate VPC for this tutorial to simplify the teardown (see #5).
2. Read the descriptive text _and_ commands prior to executing them because many of the pre-canned files must be edited using dynamically generated information.  I ended up doing the tutorial a second time, I think, because I executed a command that was half-baked and could not read from the newly created [ECR](https://aws.amazon.com/ecr/).
3. Keep a tab open in the Cloud9 editor (or wherever really) to stash notes on configuration items you will constantly need to replace in these JSON files or on the cmd line (ARNs, security group names, NLB DNS name).
4. CTRL(CMD)+F is your friend.  The Cloud9 editor is nice for a web IDE, but the terminal is janky.
5. Unless you are intimate with Amazon's dependency chains, be prepared to spend some time de-tangling the software defined infrastructure you created.
6. AWS has a micro or serverless service for nearly everything, BUT to use them effectively the integrator/developer/infra-duder probably needs to have a base understanding of how the services are meant to work within the context of the application.