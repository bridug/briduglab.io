Title: Containerization Lessons Learned (so far)
Date: 2019-12-21
Category: tech-ish
Tags: containers
Slug: lessons-learned-so-far

Or is it [learnt](https://writingexplained.org/learnt-vs-learned-difference)?  I have been fortunate enough to be involved in a project that is, at its heart, part of an attempt to modernize the way we conduct business (both actual _and_ IT business).  The business component of this is fascinating to me, but that is for another post on another day.  I have piled onto a few of the under represented roles including solutions architect, IT service developer, team level sysadmin and boy who cries wolf.  

### Lessons Learn(ed)(t)

1. Stakeholder desires and expectations surrounding an outcome are the most important piece of documentation to be captured.  Context is king...and these directly translate into technical requirements.
2. Containers have been around for long enough that there is legacy information floating around.  Some of this legacy information is from as recently as 18 months ago, which makes it incredibly difficult to discern old from new or good enough from best practice.
3. One Container = one service.  Mashing things together because it is "easier" really isn't.  Again, this has almost always been true but the option has been there to mash it together.  Doing so in a containerized environment is actually more difficult.
4. Everything should be on "paper", in the design, from the start.  Oh it can change as you learn, but the design _must_ be updated.  This has always been true, but with infrastructure that is built as needed and by robots it is even more important.
5. Subscribing to a zero trust model for secrets management took me from "pull the covers over my head" (paralyzed) scared, to "turn on all the lights in the house and keep moving" (cautious) scared.  I think this is a big boy move =)
