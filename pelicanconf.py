#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from datetime import date

# Author
AUTHOR = u'Brian Dugan'
AUTHOR_INTRO = u'Brian Dugan'
AUTHOR_DESCRIPTION = u'Part man, part machine, all...beef patty, special sauce, lettuce, cheese, pickles, onions – on a sesame seed bun'
AUTHOR_AVATAR = 'https://secure.gravatar.com/avatar/7bfa00b90eecde27fadb1a27c8ca342a?s=240'
AUTHOR_WEB = 'https://bridug.gitlab.io'

SITENAME = 'This guy\'s blog'
SITETITLE = 'A blog by this guy'
SITESUBTITLE = 'with two thumbs'
SITEURL = 'http://localhost:8000'

# Theme
THEME = 'theme/MinimalXY'

# Theme customizations
# MINIMALXY_CUSTOM_CSS = 'static/custom.css'
MINIMALXY_FAVICON = 'favicon.ico'
MINIMALXY_START_YEAR = 2019
MINIMALXY_CURRENT_YEAR = date.today().year

# Services
#GOOGLE_ANALYTICS = 'UA-12345678-9'
#DISQUS_SITENAME = 'johndoe'

# Social
SOCIAL = (
    ('facebook', 'http://www.facebook.com/brian.dugan.399'),
    ('gitlab', 'https://gitlab.com/bridug'),
    ('linkedin', 'http://www.linkedin.com/in/brian-dugan-77355684/'),
)

CATEGORIES_SAVE_AS = 'categories.html'
ARCHIVES_SAVE_AS = 'archives.html'
# Menu
MENUITEMS = (
     ('Categories', '/' + CATEGORIES_SAVE_AS),
     ('Archive', '/' + ARCHIVES_SAVE_AS),
)


PATH = 'content'
OUTPUT_PATH = 'public'
TIMEZONE = 'America/Los_Angeles'
DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
          ('Python.org', 'http://python.org/'),
          ('Jinja2', 'http://jinja.pocoo.org/'))

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DEFAULT_CATEGORY = 'misc'

